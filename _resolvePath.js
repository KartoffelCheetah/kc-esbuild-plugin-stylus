import path from 'node:path';

const CWD = process.cwd();

export default function resolvePath (args) {
	return path.resolve(CWD, path.relative(CWD, args.resolveDir), args.path);
}
