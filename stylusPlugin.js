import path from 'node:path';
import stylus from 'stylus';
import resolvePath from './_resolvePath.js';

const namespace = 'stylus';

/*
 * stylusUseApiCallback: https://stylus-lang.com/docs/js.html#use-fn
 */
export default function ({ stylusUseApiCallback, verbose } = {}) {
	return {
		name: 'kcEsbuildPluginStylus',
		setup ({ onResolve, onLoad }) {
			onResolve(
				{ filter: /.styl$/ },
				onResolveStylusFile,
			);
			onLoad(
				{ filter: /.*/, namespace },
				onLoadStylusFile.bind(
					undefined,
					{
						stylusUseApiCallback,
						verbose
					},
				),
			);
		},
	};
}

function onResolveStylusFile (args) {
	return {
		path: resolvePath(args),
		namespace,
	};
};
async function onLoadStylusFile ({ stylusUseApiCallback, verbose }, args) {
	const { cssContent, stylusDependencyChain } = await new Promise((resolve, reject) => {
		try {
			const stylusRenderer = (stylus(`@import "${args.path}"`)
				.set('filename', args.path)
				[stylusUseApiCallback
					? 'use'
					: 'valueOf'
				](stylusUseApiCallback)
			);
			stylusRenderer.render((err, cssContent) => {
				if (err) {
					if (verbose) {
						console.error(err);
					}
					reject(err);
				}
				resolve({ cssContent, stylusDependencyChain: stylusRenderer.deps() });
			});
		} catch (err) {
			console.error(err);
			reject(err);
		}
	});
	return {
		contents: cssContent,
		loader: 'css',
		resolveDir: path.dirname(args.path),
		watchFiles: stylusDependencyChain,
	};
}
